// Problem link: https://www.interviewbit.com/problems/flip-array/

// Problem can be rephrased as creating two subsets
// such that difference between their subset sums is minimum possible
int Solution::solve(const vector<int> &A) {
    int sum = 0, n = A.size();
    for(int x : A) sum += x;
    int need = sum/2; // we need highest subset sum <= need, only then the difference of two subsets will be minimum
    vector<vector<int>> dp(n+1, vector<int>(need+1, -1));
    // dp[i][j]: minimum no. of flips needed to obtain subset with sum = j using first i elements
    //          -1 if not possible
    dp[0][0] = 0; // Base case
    for(int i = 1; i <= n; i++) {
        for(int j = 0; j <= need; j++) {
            dp[i][j] = dp[i-1][j]; // CASE 1: Exclude i-th element in our subset
            if(j-A[i-1] >= 0 && dp[i-1][j-A[i-1]] != -1) {
                if(dp[i][j] == -1 || dp[i][j] > dp[i-1][j-A[i-1]] + 1)
                    dp[i][j] = dp[i-1][j-A[i-1]] + 1; // CASE 2: Include i-th element in our subset
            }
        }
    }
    for(int i = need; i >= 0; i--) {
        if(dp[n][i] != -1) return min(dp[n][i], n - dp[n][i]);
    }
    return -1;
}
// TC: O(n*MAX_SUM)
// SC: O(n*MAX_SUM)

// Note: Space can be optimized to O(MAX_SUM) by using two dp-arrays as i-th row is dependent only on (i-1)-th row
