// Problem link: https://www.interviewbit.com/problems/distinct-subsequences/

int Solution::numDistinct(string A, string B) {
    int m = A.length(), n = B.length();
    vector<vector<int>> dp(m+1, vector<int>(n+1, 0));
    // dp[i][j] would store the no. of ways A[0...i-1] can form a subsequence identical to B[0...j-1]
    // dp[i][j] = dp[i-1][j] // i.e. not using the last character to form B[0..j] ------- CASE 1
    //          + dp[i-1][j-1] if last characters match // i.e. using the last character to include in the subsequence
    //                                                  // which can be done only if last characters match ----- CASE 2
    for(int i = 0; i <= m; i++) dp[i][0] = 1; // Base case
    for(int i = 1; i <= m; i++) {
        for(int j = 1; j <= n; j++) {
            dp[i][j] = dp[i-1][j]; // CASE 1
            if(A[i-1] == B[j-1]) dp[i][j] += dp[i-1][j-1]; // CASE 2
        }
    }
    return dp[m][n]; // since we want to know the no. of ways A[0...m-1] can form subsequence identical to B[0...n-1]
}
// TC: O(mn)
// SC: O(mn)

/* --------------------------------------------------------- */

// Space-optimized version
int Solution::numDistinct(string A, string B) {
    int m = A.length(), n = B.length();
    vector<int> dp(n+1, 0);
    dp[0] = 1; // Base case
    for(int i = 1; i <= m; i++) {
        for(int j = n; j > 0; j--) { // we need to traverse j backwards since dp[i][j] depends on dp[i-1][j] and dp[i-1][j-1]
                                    // and if we traverse forwards,
                                    // we would be overwriting the previous results we need to fill the table
            // dp[j] = dp[j]; // CASE 1
            if(A[i-1] == B[j-1]) dp[j] += dp[j-1]; // CASE 2
        }
    }
    return dp[n];
}
// TC: O(mn)
// SC: O(n)
