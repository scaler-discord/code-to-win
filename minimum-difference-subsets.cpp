Problem link: https://www.interviewbit.com/problems/minimum-difference-subsets/

// Similar to flip-array (easier version)
int Solution::solve(vector<int> &A) {
    int sum = 0, n = A.size();
    for(int x : A) sum += x;
    int need = sum/2; // we need highest subset sum <= need, only then the difference of two subsets will be minimum
    vector<vector<bool>> dp(n+1, vector<bool>(need+1, false));
    // dp[i][j]: whether it is possible to create subset with sum = j using first i elements
    dp[0][0] = true; // Base case
    for(int i = 1; i <= n; i++) {
        for(int j = 0; j <= need; j++) {
            dp[i][j] = dp[i-1][j]; // CASE 1: Exclude i-th element in our subset
            if(j-A[i-1] >= 0 && dp[i-1][j-A[i-1]]) {
                    dp[i][j] = true; // CASE 2: Include i-th element in our subset
            }
        }
    }
    int ans = sum;
    for(int i = need; i >= 0; i--) {
        if(dp[n][i]) ans = min(ans, sum - 2*i);
    }
    return ans;
}
// TC: O(n*MAX_SUM)
// SC: O(n*MAX_SUM)

// Note: Space can be optimized to O(MAX_SUM) by using two dp-arrays as i-th row is dependent only on (i-1)-th row
