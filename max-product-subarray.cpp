// Problem link: https://www.interviewbit.com/problems/max-product-subarray/

int Solution::maxProduct(const vector<int> &A) {
    // ans: answer we found so far
    // p_max: max product if prev element is used
    // p_min: min product if prev element is used
    int ans = INT_MIN, p_max = 1, p_min = 1;

    for(int x : A) {
        // c_max: max product if cur element is used
        // c_min: min product if cur element is used
        // if cur element is negative, max can be obtained by multiplying it to min so far, and similarly for min
        // if cur element is positive, max can be obtained by multiplying it to max so far, and similary for min
        int c_max = x >= 0 ? max(p_max*x, x) : max(p_min*x, x);
        int c_min = x >= 0 ? min(p_min*x, x) : min(p_max*x, x);

        ans = max(ans, c_max);
        p_max = c_max;
        p_min = c_min;
    }
    return ans;
}
// TC: O(n)
// SC: O(1)
